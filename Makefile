.POSIX:

SRC=	useradd.c
BIN=	useradd32.dll useradd64.dll useradd32.exe useradd64.exe


_CXX=	w64-mingw32-g++
LIBS=	-lnetapi32

CXX32=	i686-${_CXX}
CXX64=	x86_64-${_CXX}


all: ${BIN}

useradd32.dll: useradd.c
	${CXX32} -shared -o ${@} ${<} ${LIBS}
useradd64.dll: useradd.c
	${CXX64} -shared -o ${@} ${<} ${LIBS}
useradd32.exe: useradd.c
	${CXX32} -o ${@} ${<} ${LIBS}
useradd64.exe: useradd.c
	${CXX64} -o ${@} ${<} ${LIBS}

clean:
	rm -f ${BIN}
