# useradd.dll

Partial rewrite of https://github.com/newsoft/adduser

It allows for creation of a local user account part of one or more
groups.


## Binaries

```
Username:	audit
Password:	Test123456789!
Groups:		Administrators, Remote Desktop Users, Remote Management Users
```

https://gitlab.com/pgregoire-ci/useradd/-/jobs/artifacts/main/raw/useradd32.dll?job=build

https://gitlab.com/pgregoire-ci/useradd/-/jobs/artifacts/main/raw/useradd64.dll?job=build

https://gitlab.com/pgregoire-ci/useradd/-/jobs/artifacts/main/raw/useradd32.exe?job=build

https://gitlab.com/pgregoire-ci/useradd/-/jobs/artifacts/main/raw/useradd64.dll?job=build
